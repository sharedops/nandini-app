import { Component, ViewChild, OnInit } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";

@Component({
  selector: "app-registration",
  templateUrl: "./registration.component.html",
  styleUrls: ["./registration.component.scss"]
})
export class RegistrationComponent implements OnInit {
  registrationform: FormGroup;

  constructor(public fb: FormBuilder) {}

  ngOnInit(): void {
    this.reactiveForm();
  }

  /* Reactive form */
  reactiveForm() {
    this.registrationform = this.fb.group({
      firstname: [""],
      lastname: [""],
      surname: [""],
      email: [""],
      password: [""]
    });
  }

  submitForm() {
    console.log(this.registrationform.value);
  }
}
